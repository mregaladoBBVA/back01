package net.techu.api;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

public class ProductNortwind implements Serializable{
    @JsonProperty("ID")
    private String ID;
    @JsonProperty("Name")
    private String Name;
    @JsonProperty("Price")
    private String Price;
}
