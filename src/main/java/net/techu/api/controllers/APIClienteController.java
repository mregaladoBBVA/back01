package net.techu.api.controllers;

import net.techu.api.GestorREST;
import net.techu.api.ListaClientesNortwind;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
public class APIClienteController {
    @GetMapping(value = "/apiclientes/edad={edad}")
    public ResponseEntity<ListaClientesNortwind> getClientesDePais(@PathVariable("edad") String edad){
        final String uri = "https://services.odata.org/V4/OData/OData.svc/PersonDetails?$select=PersonID,Age,Phone";
        GestorREST gestor = new GestorREST();
        RestTemplate plantilla = gestor.crearClienteREST(new RestTemplateBuilder());
        // Aqui se hace la operación de llamar a la URI sin parámetros
        ListaClientesNortwind Lista = plantilla.getForObject(uri+"&$filter=Age eq " + String.valueOf(edad),ListaClientesNortwind.class);
        return new ResponseEntity<ListaClientesNortwind>(Lista, HttpStatus.OK);
    }
}
