package net.techu.api.controllers;

import net.techu.api.models.Vehiculo;
import net.techu.api.services.VehiculoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("vehiculos")
public class VehiculosController {
   private final VehiculoService vehiculoService;
   private Vehiculo vehiculo;

   @Autowired
   public  VehiculosController (VehiculoService vehiculoService){
       this.vehiculoService = vehiculoService;
   }
   @RequestMapping(method = RequestMethod.GET)
   public ResponseEntity<List<Vehiculo>> vehiculos(){
       System.out.println("Voy a traer la lista de los vehículos:");
       return ResponseEntity.ok(vehiculoService.findAll());
   }
   // Para que funcione el método Post en la herramienta PostMan
   @PostMapping()
    public ResponseEntity<Vehiculo> saveVehiculo(@RequestBody Vehiculo vehiculo){
       return ResponseEntity.ok(vehiculoService.saveVehiculo(vehiculo));
   }
}
