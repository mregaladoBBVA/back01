package net.techu.api.controllers;

import net.techu.api.GestorREST;
import net.techu.api.ListaProductNorthwind;
import net.techu.api.ProductNortwind;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
@RestController
public class APIProductsController {

    @GetMapping(value = "/apiproducts")
    public ResponseEntity<ListaProductNorthwind> getProducts(){
        final String uri = "https://services.odata.org/V4/OData/OData.svc/Products?$select=ID,Name,Price";
        GestorREST gestor = new GestorREST();
        RestTemplate plantilla = gestor.crearClienteREST(new RestTemplateBuilder());
        // Aqui se hace la operación de llamar a la URI sin parámetros
        //ResponseEntity<String> response = plantilla.exchange(uri, HttpMethod.GET,null,String.class);
        //return response.getBody();
        // Aquí se hace la llamada con parámetros de entrada
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.set("Usuario","AppiTechU");
        HttpEntity<String> entity = new HttpEntity<String>(headers);
        ResponseEntity<ListaProductNorthwind> respuesta = plantilla.exchange(uri, HttpMethod.GET, null, new ParameterizedTypeReference<ListaProductNorthwind>(){});
        return respuesta;
  //      try {
  //          ResponseEntity<ProductNortwind> response = plantilla.exchange(uri, HttpMethod.GET, entity, ProductNortwind.class);
   //         return response.getBody();
   //     } catch (RestClientException e){
   //         //return e.getMessage();
   //         System.out.println(e.getMessage());
   //         return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
   //     }
    }
}
