package net.techu.api.repository;

import net.techu.api.models.Vehiculo;

import java.util.List;

public interface VehiculoRepository {

    List<Vehiculo> findAll();
    public Vehiculo findOne(String id);
    public Vehiculo saveVehiculo(Vehiculo veh);
    public void updateVehiculo(Vehiculo veh);
    public void deleteVehiculo(String id);
}
